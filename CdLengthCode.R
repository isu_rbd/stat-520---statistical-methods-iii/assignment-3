
rm(list = ls(all = TRUE))

##### Generate data to create Cadmium example

length <- runif(97,  90,  200)
hist(length)

etas <- 2.3 + 0.027*length
mus <- exp(etas)

a <- 9.028
s <-mus/a

Cd <- rgamma(97, shape = a, scale = s)

#### Plot simulated Cadmium vs. length

########  Read in data set generated for slides  (optional, can skip and use generated data)

CdDat <- read.csv("CadmiumDataGeneratedForChapter4Slides.csv")
head(CdDat)
Cd <- CdDat[,2]
length <- CdDat[,1]

plot(length, Cd, pch = 19, cex = 1.5, xlab = "Length (mm)", ylab = "Cd Concentration (ng/g wet wt.)")

##### Store generated length and Cadmium in data frame

CdLengthdat <- data.frame(Length = length, Cd = Cd)

##### Plot log group standard deviations against log group means, and consider a few different group sizes

CdCat <- cut(Cd, breaks = quantile(Cd, prob =c(0, seq(0.1, 1, by = 0.1))), include.lowest = TRUE)
table(CdCat)

GroupSD <- tapply(Cd, CdCat, sd)
GroupMean <- tapply(Cd, CdCat, mean)

plot(log(GroupMean), log(GroupSD), pch = 19, cex = 1.5, xlab = "Log of Group Mean", ylab = "Log of Group SD")
lmsdmu <- lm(log(GroupSD)~log(GroupMean))
abline(a = coef(lmsdmu)[1], b= coef(lmsdmu)[2])


CdCat5 <- cut(Cd, breaks = quantile(Cd, prob =c(0, seq(0.2, 1, by = 0.2))), include.lowest= TRUE)
sum(table(CdCat5))
length(length)

GroupSD5 <- tapply(Cd, CdCat5, sd)
GroupMean5 <- tapply(Cd, CdCat5, mean)

plot(log(GroupMean5), log(GroupSD5), pch = 19, cex = 1.5, xlab = "Log of Group Mean", ylab = "Log of Group SD")
lmsdmu5 <- lm(log(GroupSD5)~log(GroupMean5))
abline(a = coef(lmsdmu5)[1], b= coef(lmsdmu5)[2])

CdCat20 <- cut(Cd, breaks = quantile(Cd, prob =c(0, seq(0.05, 1, by = 0.05))), include.lowest = TRUE)
table(CdCat20)

GroupSD20 <- tapply(Cd, CdCat20, sd)
GroupMean20 <- tapply(Cd, CdCat20, mean)

plot(log(GroupMean20), log(GroupSD20), pch = 19, cex = 1.5, xlab = "Log of Group Mean", ylab = "Log of Group SD")
lmsdmu20 <- lm(log(GroupSD20)~log(GroupMean20))
abline(a = coef(lmsdmu20)[1], b= coef(lmsdmu20)[2])

summary(lmsdmu20)$coef[,c(1,2)]


##### Selection of the systematic component
plot(length, log(Cd), xlab = "Length (mm)", ylab = "log Cd Concentration")
plot(length, -1/Cd, xlab = "Length (mm)", ylab = "Negative Reciprocal of Cd Concentration")


######  Alternative approach to assessing possible systematic components and mean-variance relationships

###########  Consider a nonparametric fit for the conditional expectation of Y given x:
plot(CdLengthdat$Length, CdLengthdat$Cd)
lowessfit <- lowess(CdLengthdat$Cd[order(CdLengthdat$Length)]~CdLengthdat$Length[order(CdLengthdat$Length)])
lines(lowessfit$x, lowessfit$y)

residualLogAbsNP <- log(abs(CdLengthdat$Cd[order(CdLengthdat$Length)] - lowessfit$y) )
plot(log(lowessfit$y), residualLogAbsNP ) 
summary(lm(residualLogAbsNP~log(lowessfit$y)))


#####  Construct estimates for a gamma distribution using glm
gammafit <- glm(Cd~length, family = Gamma(link = "log"))
summary(gammafit)

##### Verify that we obtain the same estimates using different procedures that we learned in Chapter 3 to compute the maximum liklihood esitmate
#########  To use the material in this section of the code, source an additional R program that contains R functions to compute the log likelihood, the derivative of the log likelihood, and the second derivative
source("LikelihoodFunctionsForCadmiumExample.R")
source("newtraph.R")
##### To begin, we will need starting values:
lm(log(Cd) ~length)

########  First, consider using the function "optim." The R function "optim" finds a minimum. We Require a function for the negative log likelihood
optim(c(2.51074, 0.02547), negloglikgammaglm, XYdata = cbind(length, Cd), hessian = TRUE)

########  Second, consider using the function "newtraph"
nrests <- newtraph(derforNewtRaph, data.frame(length, Cd), c(2.5 , 0.03))
sqrt(diag(nrests[[3]] ))

######## Consider three distributions: gamma, normal, inverse gaussian, each with log link

gammafit <- glm(Cd~length, family = Gamma(link = "log"))
normalfit <- glm(Cd~length, family =  gaussian(link = "log"))
inversegaussianfit <- glm(Cd~length, family = inverse.gaussian(link = "log"))

########## Extract the coefficients for each model and put them in a table:
library("xtable")
estse <- rbind(as.vector(t(summary(gammafit)$coefficients[,c(1,2)])),as.vector(t( summary(normalfit)$coefficients[,c(1,2)])), as.vector(t(summary(inversegaussianfit)$coefficients[,c(1,2)])))
xtable(estse, digits = 3)

#########  Construct 95% Wald intervals for regression parameters, and put them in a table
intgamma <- cbind( summary(gammafit)$coefficients[,c(1)] - 1.96*summary(gammafit)$coefficients[,c(2)] ,
 summary(gammafit)$coefficients[,c(1)] +1.96*summary(gammafit)$coefficients[,c(2)])

intnormal <- cbind( summary(normalfit)$coefficients[,c(1)] -1.96*summary(normalfit)$coefficients[,c(2)] ,
 summary(normalfit)$coefficients[,c(1)]+ 1.96*summary(normalfit)$coefficients[,c(2)])

intig <- cbind( summary(inversegaussianfit)$coefficients[,c(1)] - 1.96*summary(inversegaussianfit)$coefficients[,c(2)] ,
 summary(inversegaussianfit)$coefficients[,c(1)]+ 1.96*summary(inversegaussianfit)$coefficients[,c(2)])

rbind(as.vector(t(intgamma)), as.vector(t(intnormal)), as.vector(t(intig)))

#########  Estimate the dispersion parameter for each model using the method of moments estimator:

######  First, calculate by hand, and then use the built-in summary() function in R:
rtestforgamma <- (Cd - gammafit$fitted)/sqrt(gammafit$fitted^2)
sum(rtestforgamma^2)/95
summary(gammafit)$disperson
1/(sum(rtestforgamma^2)/95)
1/summary(gammafit)$disperson

######  First, calculate by hand, and then use the built-in summary() function in R:
rtestfornormal <- (Cd - normalfit$fitted)
(sum(rtestfornormal^2)/95)
summary(normalfit)$dispersion
1/(sum(rtestfornormal^2)/95)

######  First, calculate by hand, and then use the built-in summary() function in R:
rtestofrig <- (Cd - inversegaussianfit$fitted)/sqrt(inversegaussianfit$fitted^3)
(sum(rtestofrig^2)/95)
summary(inversegaussianfit)$dispersion
1/(sum(rtestofrig^2)/95)
1/summary(inversegaussianfit)$dispersion


#########  Calculate the scaled deviance for each model:

##### Scaled deviance for gamma:
2/(sum(rtestforgamma^2)/95)*sum( (Cd - gammafit$fitted)/gammafit$fitted - log(Cd/gammafit$fitted))
##### Unscaled deviance for gamma:
summary(gammafit)$deviance

##### Scaled deviance for normal
2/(sum(rtestfornormal^2)/95)*sum( (Cd - normalfit$fitted)/normalfit$fitted - log(Cd/normalfit$fitted))
##### Unscaled deviance for normal
summary(normalfit)$deviance

##### Scaled deviance for inverse-Gaussian
summary(inversegaussianfit)$deviance/summary(inversegaussianfit)$dispersion
#####  Unscaled deviance for inverse-Gaussian
summary(inversegaussianfit)$deviance

###############  Combine unscaled ans scaled deviances in a table:
col1dev <- c(summary(gammafit)$deviance, summary(gammafit)$deviance/summary(gammafit)$dispersion)
col2dev <- c(summary(normalfit)$deviance, summary(normalfit)$deviance/summary(normalfit)$dispersion)
col3dev <- c(summary(inversegaussianfit)$deviance, summary(inversegaussianfit)$deviance/summary(inversegaussianfit)$dispersion)

cbind(col1dev, col2dev, col3dev)

############  Standardized deviance residuals:

#### H matrix for gamma:
X <- cbind(1, length)

jpeg("GammaDevResCd.jpeg")
Hii <- diag(X%*%solve(t(X)%*%X)%*%t(X))
dsdgam <- residuals(gammafit, type = "deviance")/sqrt(summary(gammafit)$dispersion)/sqrt(1 - Hii)
plot(gammafit$fitted, dsdgam, xlab = "Fitted Value (ng/g)", ylab = "Standardized Deviance Residual")
abline(h = 0)
dev.off()

jpeg("NormalDevResCd.jpeg")
Znorm <- diag(sqrt(normalfit$weights))%*%X
Hiinorm <-diag(Znorm%*%solve(t(Znorm)%*%Znorm)%*%t(Znorm))
dsdnorm<- residuals(normalfit, type = "deviance")/sqrt(summary(normalfit)$dispersion)/sqrt(1 - Hiinorm)
plot(normalfit$fitted, dsdnorm, xlab = "Fitted Value (ng/g)", ylab = "Standardized Deviance Residual")
abline(h = 0)
dev.off()

jpeg("IGDevResCd.jpeg")
Zig <- diag(sqrt(inversegaussianfit$weights))%*%X
HiiIG<-diag(Zig%*%solve(t(Zig)%*%Zig)%*%t(Zig))
dsdig<- residuals(inversegaussianfit, type = "deviance")/sqrt(summary(inversegaussianfit)$dispersion)/sqrt(1 - HiiIG)
plot(inversegaussianfit$fitted, dsdig, xlab = "Fitted Value (ng/g)", ylab = "Standardized Deviance Residual")
abline(h = 0)
dev.off()

##### Confidence intervals for the mean
estseMugam <- predict(gammafit, se.fit = TRUE, type = "response")
lowerMugam <- estseMugam$fit - 1.96*estseMugam$se
upperMugam <- estseMugam$fit + 1.96*estseMugam$se
############  Coompare your standard errors for the mean to the "by hand" method:
cbind(sqrt(diag(diag(estseMugam$fit)%*%cbind(1, length)%*%vcov(gammafit)%*%t(cbind(1,length))%*%diag(estseMugam$fit))), estseMugam$se)

#####  Plot the confidence interals for the mean
plot(length, Cd, xlab = "Length (mm)", ylab = "Cadmium Concentration ng/g", pch = 19, cex = 1.5)
lines(length[order(length)], estseMugam$fit[order(length)]) 
lines(length[order(length)], lowerMugam[order(length)], lty = 2) 
lines(length[order(length)], upperMugam[order(length)], lty = 2)
 
